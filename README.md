# The Azamon Project

This is 1995, and Azamon is about to start an internet revolution.

## Getting Started

There are two parts to this project. You can find the web version live at: . The cli version must be run locally. Please download this project.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
npm install
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Database struture

```
users - A User model consists of username, encrypted password, and an array of bookIds that the person has ordered.
books - A Book model consits of title, author, isbn, width, and height. It does not have information on the numbers that are available or ordered. Each unique book will not repeat twice, and can be looked up by its unique isbn.
physicalBooks - A model containiing the actual bookId string, and isbn number unique to each Book model.
instocks - This collection lists the number and identiy of books that are currently available in stock, model contains only of bookId.
orders - THis collection is a list of books that have been ordered by customers. It does not store customer info, only stores bookId.
sessions - Stores user sessions.

```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [React](https://reactjs.org/) - Frontend Framework
* [Commandjs](https://github.com/tj/commander.js/) - Node.js CLI Tool
* [MongoDB](https://www.mongodb.com/) - Database
* [Node.js](https://nodejs.org/en/) - Server


## Authors

* **Claire Peng** - *Initial work* - [website](https://clairepeng.ca)
