import React, { Component } from "react";
import axios from "axios";

export default class Books extends Component {
  state = {
    user: "",
    order: [],
    stock: []
  };

  componentWillMount() {
    this.getStock();
    this.getOrder();
  }

  getOrder = () => {
    const payload = {
      userId: localStorage.getItem("userId")
    };
    axios
      .post("viewOrder", payload)
      .then(response => {
        if (response.status === 200) {
          this.setState({
            order: response.data.books
          });
        }
      })
      .catch(err => {
        alert(err.message);
      });
  };


  // view books in order, plus return
  displayOrder = () => {
    const books = this.state.order;
    const bookItems = books.sort().map(book => (
      <li className="collection-item" key={book._id}>
        {book.bookDetail.title}{" "}
        <button onClick={() => this.returnOrder(book._id)}>Return</button>
      </li>
    ));
    return (
      <div>
        {bookItems.length > 0 ? "" : "You have not ordered any."}
        <ul className="collection">{bookItems}</ul>
      </div>
    );
  };

  getStock = () => {
    axios
      .get("book/inStock")
      .then(response => {
        console.log(response);
        if (response.status === 200) {
          this.setState({
            stock: response.data.books
          });
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };

  orderOrReturnBook = (bookId, path) => {
    const payload = {
      session: localStorage.getItem("userId"),
      bookId
    };
    axios
      .post(path, payload)
      .then((response) => {
        if(response.status === 200) {
          this.getOrder();
          this.getStock();
        }
      })
      .catch(err => {
        console.log(err.message);
      });
  };

  orderBook = bookId => {
    return this.orderOrReturnBook(bookId, "order");
  };

  returnOrder = bookId => {
    return this.orderOrReturnBook(bookId, "return");
  };

  // order books
  displayStock = () => {
    const books = this.state.stock;
    const bookItems = books.sort().map(book => (
      <li className="collection-item" key={book.bookId}>
        {book.title}
        <button onClick={() => this.orderBook(book.bookId)}>Order</button>
      </li>
    ));
    return (
      <div>
        {bookItems.length > 0 ? "" : "We have no more books in stock"}
        <ul className="collection">{bookItems}</ul>
      </div>
    );
  };

  render() {
    return (
      <div style={style}>
        <table>
          <thead>
          <tr>
            <th>Your Order</th>
            <th>Our Stock</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>
              {this.displayOrder()}
            </td>
            <td>
              {this.displayStock()}
            </td>
          </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

const style = {
  textAlign: 'center',
  listStyleType: 'none'
}